import requests
import os
import time
import logging
import sys
import hashlib
import winreg
import ctypes.wintypes
from watchdog.observers import Observer
from watchdog.events import LoggingEventHandler
from watchdog.events import FileSystemEventHandler
from requests_toolbelt.multipart.encoder import MultipartEncoder
from peewee import *
from pathlib import Path

REG_PATH = r"Software\\aramranked"


def set_reg(name, value):
    try:
        winreg.CreateKey(winreg.HKEY_CURRENT_USER, REG_PATH)
        registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, REG_PATH, 0,
                                      winreg.KEY_WRITE)
        winreg.SetValueEx(registry_key, name, 0, winreg.REG_SZ, value)
        winreg.CloseKey(registry_key)
        return True
    except WindowsError:
        return False


def get_reg(name):
    try:
        registry_key = winreg.OpenKey(winreg.HKEY_CURRENT_USER, REG_PATH, 0,
                                      winreg.KEY_READ)
        value, regtype = winreg.QueryValueEx(registry_key, name)
        winreg.CloseKey(registry_key)
        return value
    except WindowsError:
        return False


def MyDocPath():
    CSIDL_PERSONAL = 5       # My Documents
    SHGFP_TYPE_CURRENT = 0   # Get current, not default value

    buf = ctypes.create_unicode_buffer(ctypes.wintypes.MAX_PATH)
    ctypes.windll.shell32.SHGetFolderPathW(
        None, CSIDL_PERSONAL, None, SHGFP_TYPE_CURRENT, buf)

    return buf.value


def CreateHash(f):
    # BUF_SIZE is totally arbitrary, change for your app!
    BUF_SIZE = 65536  # lets read stuff in 64kb chunks!

    sha256 = hashlib.sha256()
    while True:
        data = f.read(BUF_SIZE)
        if not data:
            break
        sha256.update(data)
    return sha256.hexdigest()


def GetAccount(src_path):
    ls = "Accounts"
    rs = "2-Hero"
    left = src_path.rfind(ls) + len(ls) + 1
    right = src_path.rfind(rs) - 1
    account = src_path[left:right]
    return account


def GetReplay(src_path):
    ls = "Multiplayer"
    rs = "StormReplay"
    left = src_path.rfind(ls) + len(ls) + 1
    right = src_path.rfind(rs) - 1
    replay = src_path[left:right]
    return replay


def GetPath(src_path):
    # get path for ...\\Multiplayer folder
    rs = "Multiplayer"
    src_path = src_path[:src_path.rfind(rs) + len(rs)]
    return src_path


def GetFiles(src_path):
    files = sorted(Path(src_path).iterdir(), key=os.path.getmtime)
    return files


def GetData(src_path):
    return [GetAccount(src_path), GetReplay(src_path)]


def GetRegPath(path):
    reg_path = get_reg('path')
    l = reg_path.split(';')
    if path not in l:
        l.append(path)
    ans = str()
    for el in l:
        ans += el + ';'
    return ans[:-1]
