from peewee import *
import os
from datetime import datetime
from utility import *

appdata = os.getenv('APPDATA')

if not os.path.isdir(appdata + "\\aramranked"):
    os.mkdir(appdata + "\\aramranked")
else:
    print("Folder aramranked already exists")

db = SqliteDatabase(appdata + '\\aramranked\\database.db')


def NewUser(account):
    User.create(username=account)
    return None


def AddToDB(account, replay, hashreplay):
    temp_user = User.select().where(User.username == account)
    try:
        Replays.create(user=temp_user, replay=replay,
                       hashreplay=hashreplay)
        return True
    except IntegrityError as e:
        print("Replay {} belongs to another user".format(replay))
        return False


def CheckNewFiles(files):
    # check whether files are in db, and add them if they are not
    new_files = list()
    for path in files:
        # print(path)
        try:
            file = open(path, 'rb')
        except PermissionError as e:
            print("File {} failed to open with {}".format(path, e))
            return list()
        [account, replay] = GetData(str(path))
        if account not in [u.username for u in User.select()]:
            NewUser(account)
        temp_user = User.select().where(User.username == account)
        hashreplay = CreateHash(file)
        if hashreplay not in [rep.hashreplay for rep in Replays.select().where(Replays.user == temp_user)]:
            new_files.append([str(path), hashreplay])
        else:
            print("Replay {} is already in the database".format(replay))
        file.close()
    return new_files


class BaseModel(Model):
    class Meta:
        database = db


class User(BaseModel):
    username = CharField(unique=True)
    rank = IntegerField(default=1600)


class Replays(BaseModel):
    user = ForeignKeyField(User, backref='replays')
    replay = CharField()
    #path = CharField(NULL=True)
    # should be unique for each account, but not unique in general
    hashreplay = CharField(unique=True)
    #is_new = BooleanField(default=True)


def OpenDatabase():
    db.connect()
    db.create_tables([User, Replays])
