from utility import *
from pathlib import Path
from database import *
from peewee import *
from requests_toolbelt.multipart.encoder import MultipartEncoder
from watchdog.events import FileSystemEventHandler
from watchdog.events import LoggingEventHandler
from watchdog.observers import Observer
# from update.py import *
import requests
import os
import time
import logging
import sys
import hashlib
import winreg
import ctypes.wintypes


db = SqliteDatabase('database.db')


def SendToServer(new_files):
    count = 1
    while True:
        session = requests.Session()
        try:
            # можно чекать все "новые" файлы и менять их тип на "старый" после отправки на сервер
            for path in new_files:
                # print(path)
                file = open(path[0], 'rb')
                #!изменить схему
                [account, replay] = GetData(path[0])
                hashreplay = path[1]
                print("Account: {}, Replay: {}".format(account, replay))

                form = MultipartEncoder(
                    {'replay': ('file', file)})
                headers = {'Content-Type': form.content_type}
                resp = session.post(
                    'https://api.aram.club/aramranked/v1/upload', headers=headers, data=form, timeout=10)
                file.seek(0)
                print(resp)

                AddToDB(account, replay, hashreplay)
            break
        except Exception as e:
            count += 1
            print(count, "try: {}".format(e))
            time.sleep(1)
        session.close()
        print("Sended with {} try".format(count))


class FileEventHandler(FileSystemEventHandler):
    def on_created(self, event):
        src_path = event.src_path
        print("Found new file {}".format(src_path))
        if src_path[-12:] == '.StormReplay':
            path = get_reg('path')
            if path == False:
                path = GetPath(src_path)
                set_reg('path', path)
            else:
                path = GetRegPath(GetPath(src_path))
                set_reg('path', path)
            # files = GetFiles(path)
            # new_files = CheckNewFiles(files)
            try:
                # print("Check")
                file = open(Path(src_path), 'rb')
                #print("File opened")
                hashreplay = CreateHash(file)
                #print("Hash calculated")
                new_files = [[src_path, hashreplay]]
                # print(new_files)
                time.sleep(1)
                SendToServer(new_files)
            except PermissionError as e:
                print(e)

        else:
            print("Found file with different extension")


if __name__ == "__main__":
    # асинхронно
    # UpdateExec()
    # if True:
    #     # запрос на сервер
    #     # получает файл и делает все что ниже
    #     os.rename(sys.executable, sys.executable+".bak")
    #     os.rename('.\client_upload.exe', sys.executable)
    #     # выполняет магию
    #     print(sys.executable)
    OpenDatabase()
    # можно сделать path листом и добавлять в него все path всех юзеров и
    # проверять при входе все папки всех юзеров
    path = get_reg('path')
    if path != False:
        path_list = path.split(';')
        for path_temp in path_list:
            print("Searching for new replays in folder {}".format(path_temp))
            files = GetFiles(path_temp)
            # print(files)
            new_files = CheckNewFiles(files)
            SendToServer(new_files)

    logging.basicConfig(level=logging.INFO,
                        format='%(asctime)s - %(message)s',
                        datefmt='%Y-%m-%d %H:%M:%S')
    env = MyDocPath()
    if env == None:
        raise "!!!ERROR ERROR ERROR!!!"

    src_path = env + """\\Heroes of the Storm\\Accounts\\"""
    # print(path)

    event_handler = FileEventHandler()
    observer = Observer()
    observer.schedule(event_handler, src_path, recursive=True)
    observer.start()
    try:
        while True:
            time.sleep(1)
    except KeyboardInterrupt:
        observer.stop()
    observer.join()
