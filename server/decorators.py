import time
import functools

# renames function
def rename(newname):
    def decorator(func):
        func.__name__ = newname
        return func
    return decorator

def Benchmark(func):
    @rename(func.__name__)
    def wrapper(*args, **kwargs):
        start = time.time()
        return_value = func(*args, **kwargs)
        end = time.time()
        print('[*] {}::Benchmark: executed in {:.3f} seconds'.format(func.__name__, end-start))
        return return_value
    return wrapper

def StaticVar(**kwargs):
    def decorate(func):
        for k in kwargs:
            setattr(func, k, kwargs[k])
        return func
    return decorate

# Caches function result for timeout seconds
class TimedCache():
    def __init__(self, timeout: int):
        self.timeout = timeout
        self.updated = 0

    def __call__(self, func):
        def decorate(*args, **kwargs):
            curTime = time.time()
            if curTime - self.timeout > self.updated:
                self.results = func(*args, **kwargs)
                self.updated = curTime
                print('[*] {}::TimedCache: updating'.format(func.__name__))
                return self.results
            else:
                print('[*] {}::TimedCache: using cache'.format(func.__name__))
                return self.results
        return decorate
