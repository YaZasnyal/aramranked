import logging

from database import *
from match import *
from matchmaking import *
from decorators import *
from exceptions import GoodException

def WriteMatch(initData, details):
    replay = Replay(initData, details)
    if (replay.timestamp <= GetSeasonStartTimestamp()):
        raise GoodException('Replay belogs to the ended season')

    match = Match.get_or_none(fingerprint = replay.fingerprint)
    if match is not None:
        print("Replay with such fingerprint ({}) already exists".format(replay.fingerprint))
        return
    match = Match.create(
        fingerprint = replay.fingerprint,
        timestamp = replay.timestamp,
    )
    replay.matchId = match.id

    #create users
    WritePlayers(replay)
    CalcNewRank(replay)
    WriteParticipants(replay)
    UpdatePlayerRank(replay)
    #calc and update raitings
            

def WritePlayers(replay):
    for player in replay.players:
        dbPlayer = User.get_or_none(bnetid = player.id, region = player.region, realm = player.realm)
        if dbPlayer is None:
            dbPlayer = User.create(
                bnetid = player.id, 
                region = player.region, 
                realm = player.realm,
                username = player.username,
                rank = 1600
            )
        player.rank = dbPlayer.rank
        player.dbId = dbPlayer.id
       

def CalcNewRank(replay: dict):
    CalculateRanks(replay.players)

def WriteParticipants(replay):
    for player in replay.players:
        hero = Heroes.get_or_none(name = player.hero)
        if hero is None:
            heroAlias = HeroAliases.get_or_none(alias = player.hero)
            if heroAlias is None:
                hero = Heroes.get_or_none(name = "UNDEFINED")
            else:
                hero = Heroes.get_or_none(id = heroAlias.name)
        player.heroId = hero.id
    
    if all(player.heroId == 1 for player in replay.players ):
        raise Exception("Unknown language of replay")
    
    for player in replay.players:
        MatchParticipants.create(
            match_id = replay.matchId,
            user_id = player.dbId,
            hero = player.heroId,
            winner = player.winner,
            old_rank = player.rank,
            new_rank = player.newRank
        )

def UpdatePlayerRank(replay):
    for player in replay.players:
        dbPlayer = User.get_or_none(bnetid = player.id, region = player.region, realm = player.realm)
        dbPlayer.rank = player.newRank
        dbPlayer.save()

@TimedCache(timeout=3600)
def GetSeasonStartTimestamp():
    seasons = Season.select().order_by(Season.start.desc()).limit(1)
    return seasons[0].start
