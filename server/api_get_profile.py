from database import *
import json
import logging
import math
import functools

from decorators import *

def ApiGetProfile(id, page, pagesize):
    result = {}
    user = User.get_or_none(id = id)
    result['uid'] = user.id
    result['name'] = user.username
    result['rank'] = user.rank

    matches = (MatchParticipants.select()
                .join(Match)
                .switch(MatchParticipants)
                .join(Heroes)
                .where(MatchParticipants.user_id == id)
                .order_by(Match.timestamp.desc()))
    
    result['matches'] = []

    if page > math.ceil(len(matches)/pagesize):
        page = math.ceil(len(matches)/pagesize)
    numOfElements = pagesize if len(matches) - pagesize*page > pagesize else len(matches) - pagesize*page
    for i in range(0, numOfElements):
        j = pagesize*(page)
        result['matches'].append({
            'old_rank': matches[j+i].old_rank,
            'new_rank': matches[j+i].new_rank,
            'winner': matches[j+i].winner,
            'hero': matches[j+i].hero.name,
            'match_id': matches[j+i].match_id.id,
            'timestamp': matches[j+i].match_id.timestamp
        })
        
    result['total_matches'] = len(matches)

    return json.dumps(result)

@functools.lru_cache(maxsize=100)
def ApiGetMatchDetaild(id):
    result = []

    participants = (MatchParticipants.select()
                    .join(User)
                    .switch(MatchParticipants)
                    .join(Heroes)
                    .where(MatchParticipants.match_id == id)
                    .order_by(MatchParticipants.winner.desc())
    )

    for player in participants:
        result.append({
            'id': player.user_id.id,
            'name': player.user_id.username,
            'winner': player.winner,
            'hero': player.hero.name,
            'old_rank': player.old_rank,
            'new_rank': player.new_rank
        })
    return json.dumps(result)

@functools.lru_cache(maxsize=200)
def ApiGetPlayer(name):
    result = []
    name = name + "%"
    users = User.select().where(User.username ** name).limit(5)
    for user in users:
        print(user.username)
        result.append({'id': user.id, 'name': user.username})

    return json.dumps(result)

@TimedCache(timeout=600)
@Benchmark
def ApiGetHeroStatistics():
    result = []

    wins_sq = (MatchParticipants.select(MatchParticipants.hero, fn.COUNT(MatchParticipants).alias('wins'))
                                .where(MatchParticipants.winner == True)
                                .group_by(MatchParticipants.hero))
    herostats = (MatchParticipants
                  .select(MatchParticipants, wins_sq.c.wins, fn.COUNT(MatchParticipants).alias('plays'))
                  .join(Heroes)
                  .switch(MatchParticipants)
                  .join(wins_sq, on=(MatchParticipants.hero == wins_sq.c.hero_id))
                  .group_by(MatchParticipants.hero)
                )
    
    for herostat in herostats:
        result.append({
            'name': herostat.hero.name,
            'games': herostat.plays,
            'wins': herostat.matchparticipants.wins,
            'winrate': herostat.matchparticipants.wins/herostat.plays
        })
    return json.dumps(result)
