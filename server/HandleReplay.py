import json
import os
import sys
import subprocess
import logging
from flask import Flask, request
from werkzeug.utils import secure_filename
from exceptions import GoodException

from db_worker import *

python2Path = 'python2'
if sys.platform == 'win32':
    python2Path = 'C:\\Python27\\python'

heroProtocolPath = 'third_party/heroprotocol/heroprotocol.py'

def ProcessReplay(fileName: str):
    initData = DecodeInitdata(ExecuteCommand('initdata', fileName))
    details = DecodeDetails(ExecuteCommand('details', fileName))

    CheckBrawl(initData)
    WriteMatch(initData, details)
    
    pass

def ExecuteCommand(option: str, fileName: str):
    result = subprocess.Popen('{} {} --{} --json {}'.format(python2Path, heroProtocolPath, option, fileName), 
                              shell=True, stdout=subprocess.PIPE).communicate()[0].decode('ISO-8859-1')
    result = result[result.find('\n{') + 1:]
    
    return json.loads(result)

# Декодирует конкретную строку
def DecodeString(string):
    return string.encode('ISO-8859-1').decode('utf-8')

def DecodeDetails(details):
    details['m_title'] = DecodeString(details['m_title'])
    for player in details['m_playerList']:
        player['m_name'] = DecodeString(player['m_name'])
        player['m_hero'] = DecodeString(player['m_hero'])
    return details

def DecodeInitdata(initdata):
    return initdata

# Проверяет является ли игра потасовкой и бросает исключение, если это не так
def CheckBrawl(initdata):
    if not initdata['m_syncLobbyState']['m_gameDescription']['m_gameOptions']['m_ammId'] == 50031:
        raise GoodException("This is not a brawl")
