import io
from os import listdir
from os.path import isfile, join

from database import *

def AddHeroes():
    path = 'server/heroes'
    files = [f for f in listdir(path) if isfile(join(path, f))]
    
    translations = []
    print('Processing file en')
    AddEn(path + '/en', translations)
    for file_ in files:
        print('Processing file {}'.format(file_))
        if (file_ == 'en'):
            continue
        else:
            AddTranslation(path + '/' + file_, translations, file_)
    
    WriteToDatabase(translations)


def GetLines(path):
    result = []
    file = io.open(path, mode="r",  encoding="utf-8")
    lines = file.readlines()
    for line in lines:
        if not line == '':
            if line[len(line)-1] == '\n':
                line = line[:-1]
            result.append(line)
    return result

def AddEn(path, translations):
    lines = GetLines(path)
    print('en translation has {} heroes'.format(len(lines)))
    for line in lines:
        translations.append([line])
    

def AddTranslation(path, translations, lang):
    lines = GetLines(path)
    print('{} translation has {} heroes'.format(lang, len(lines)))
    for i, line in enumerate(lines):
        translations[i].append([lang, line])

def WriteToDatabase(translations):
    for translation in translations:
        hero = Heroes.get_or_none(name = translation[0])
        if hero is None:
            hero = Heroes.create(name = translation[0])
        for i in range(1, len(translation)):
            alias = HeroAliases.get_or_none(alias = translation[i][1])
            if alias is None:
                HeroAliases.create(
                    alias = translation[i][1],
                    name = hero.id,
                    lang = translation[i][0]
                )
