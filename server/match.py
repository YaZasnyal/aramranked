import hashlib

class Player:
    def __init__(self, player: dict):
        self.dbId = 0
        self.id = player['m_toon']['m_id']
        self.region = player['m_toon']['m_region']
        self.realm = player['m_toon']['m_realm']
        self.username = player['m_name']
        self.hero = player['m_hero']
        self.heroId = 0
        self.winner = True if player['m_result'] == 1 else False
        self.rank = 0
        self.newRank = 0

class Replay:
    def __init__(self, initdata, details):
        self.matchId = 0 # Id реплея в базе данных
        self.fingerprint = CalcFingerprint(initdata, details)
        self.players = []
        for player in details['m_playerList']:
            self.players.append(Player(player))
        self.timestamp = details['m_timeUTC']


def CalcFingerprint(initData, details) -> int:
    players = details['m_playerList']
    ids = list()
    for player in players:
        ids.append(player['m_toon']['m_id'])
    ids.sort()
    idStr = str()
    for id in ids:
        idStr += '{}'.format(id)
    idStr += '{}'.format(initData['m_syncLobbyState']['m_gameDescription']['m_randomValue'])
    return CreateHash(idStr.encode())

def CreateHash(s):
    sha256 = hashlib.sha256()
    sha256.update(s)
    return sha256.hexdigest()
