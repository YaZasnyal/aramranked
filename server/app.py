from flask import Flask, request, jsonify
from flask_cors import CORS
from HandleReplay import *
from database import *
from database_migrations import *
from heroes import *
import json
import traceback

from api_get_profile import *
from exceptions import GoodException

app = Flask(__name__)

# enable CORS
CORS(app)

@app.route('/aramranked/v1/upload', methods=['POST'])
def ProcessUpload():
    if request.method == 'POST':
        print("HandleReplay: recieved files: {}".format(request.files))
        f = request.files['replay']
        fn = secure_filename(f.filename)
        f.save(fn)

        try:
            ProcessReplay(fn)
        except GoodException as e:
            print("ProcessUpload: got good exception: {}".format(e))
        except Exception as e:
            print("ProcessUpload: got bad exception: {}".format(e))
            traceback.print_exc(file=sys.stdout)
        os.remove(fn)

        return 'Nice'

@app.route('/aramranked/v1/get_profile', methods=['GET'])
def GetProfile():
    if request.method == 'GET':
        id = request.args.get('id')
        page = request.args.get('page', 0, type = int)
        page_size = request.args.get('page_size', 10, type = int)
        print('{}, {}, {}'.format(id, page, page_size))
        try:
            return ApiGetProfile(id, page, page_size)
        except Exception as e:
            print(e)
    
    return '{}'

@app.route('/aramranked/v1/get_match_details', methods=['GET'])
def GetMatchDetaild():
    if request.method == 'GET':
        id = request.args.get('id')
        print('get_match_details: id = {}'.format(id))
        try:
            return ApiGetMatchDetaild(id)
        except Exception as e:
            print(e)
    
    return '{}'

@app.route('/aramranked/v1/get_player', methods=['GET'])
def GetPlayer():
    if request.method == 'GET':
        name = request.args.get('name', type=str)
        print('GetPlayer: name = {}'.format(name))
        try:
            return ApiGetPlayer(name)
        except Exception as e:
            print(e)
    
    return '{}'

@app.route('/aramranked/v1/get_hero_statistics', methods=['GET'])
def GetHeroStatistics():
    if request.method == 'GET':
        print('GetHeroStatistics: called')
        try:
            return ApiGetHeroStatistics()
        except Exception as e:
            print(e)
    
    return '{}'

if __name__ == '__main__':
    OpenDatabase()
    MigrateDatabase()
    AddHeroes()
    ApiGetHeroStatistics()
    # ProcessReplay('"C:\\Projects\\testtopost.StormReplay"')
    app.run(debug=True, threaded=False, host='0.0.0.0', port=25000)
