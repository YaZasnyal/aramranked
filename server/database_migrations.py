from database import *

# Create DbInfo table. Start first season. Reset ranks
def transitionNoneTo1():
    res = DbInfo.get_or_none(name='Version')
    if res is not None:
        return

    DbInfo.create(name='Version', value='1')

    Season.get_or_create(name='Preseason', start=0, stop=132341342460810202)
    Season.get_or_create(name='Season 1', start=132341342460810202, stop=0)

    users = User.select()
    for user in users:
        user.rank = 1600
        user.save()

def MigrateDatabase():
    transitionNoneTo1()
    