from peewee import *
import os

datadir = os.getenv('DATA_DIR')
if datadir is None:
    datadir = ''
db = SqliteDatabase(datadir + 'server.db')
# db = SqliteDatabase(':memory:')


class BaseModel(Model):
    class Meta:
        database = db

class DbInfo(BaseModel):
    name = CharField(unique=True)
    value = CharField()
    
class Heroes(BaseModel):
    name = CharField(unique=True)

class HeroAliases(BaseModel):
    alias = CharField(unique=True)
    name = ForeignKeyField(Heroes)
    lang = CharField()


class User(BaseModel):
    bnetid = IntegerField() # battle.net id
    region = IntegerField()
    realm = IntegerField()
    username = CharField(index=True)
    rank = IntegerField()
    join_date = DateTimeField(null = True)

    class Meta:
        indexes = (
            (('bnetid', 'region', 'realm'), True),
        )

class Season(BaseModel):
    name = CharField(unique=True)
    start = DateTimeField()
    stop = DateTimeField()

class Match(BaseModel):
    fingerprint = CharField(unique=True) # game fingerprint
    timestamp = DateTimeField() # match timestamp

class MatchParticipants(BaseModel):
    match_id = ForeignKeyField(Match)
    user_id = ForeignKeyField(User, index=True)
    hero = ForeignKeyField(Heroes)
    winner = BooleanField() # false = blue, true = red
    old_rank = IntegerField(null=True) # rank before the game
    new_rank = IntegerField(null=True) # rang after the game

    class Meta:
        indexes = (
            (('hero', 'winner'), False),
        )


def OpenDatabase():
    db.connect()
    
    db.create_tables([DbInfo, Heroes, HeroAliases, User, Match, MatchParticipants, Season])
    try:
        Heroes.create(name = "UNDEFINED")
        # Create pre season
    except Exception:
        pass
    