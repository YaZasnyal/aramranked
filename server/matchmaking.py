def CalculateRanks(players):
    R1 = 0
    R2 = 0

    K = 50  # скорость набора рейтинга

    l = len(players)  # потом можно будет заменить на l = 10

    for i in range(l):
        if players[i].winner:
            R1 += players[i].rank
        else:
            R2 += players[i].rank
    R1 = R1 / l
    R2 = R2 / l

    E1 = 1 / (1 + 10**((R2 - R1)/400))
    E2 = 1 / (1 + 10**((R1 - R2)/400))

    for i in range(l):
        if players[i].winner:
            players[i].newRank = players[i].rank + K * (1 - E1)
        else:
            players[i].newRank = players[i].rank + K * (0 - E2)
    return players
