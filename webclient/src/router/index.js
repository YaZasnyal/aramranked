import Vue from 'vue'
import Router from 'vue-router'

import Player from '@/components/Player'
import Main from '@/components/Main'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/player/:id',
      name: 'Player match history',
      component: Player
    },
    {
      path: '/',
      name: 'Main page',
      component: Main
    }
  ]
})
