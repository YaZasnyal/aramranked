// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import router from './router'
import { BootstrapVue, IconsPlugin } from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import Paginate from 'vuejs-paginate'
import Autocomplete from '@trevoreyre/autocomplete-vue'

import PlayerSearch from '@/components/PlayerSearch'

Vue.config.productionTip = false
// Install BootstrapVue
Vue.use(BootstrapVue)
// Optionally install the BootstrapVue icon components plugin
Vue.use(IconsPlugin)
Vue.use(Autocomplete)

Vue.component('paginate', Paginate)

Vue.component('PlayerSearch', PlayerSearch)
Vue.prototype.$apihost = process.env.API_URL ? process.env.API_URL : 'https://api.aram.club'

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})
